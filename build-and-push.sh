#!/bin/bash

buildAndPush () {
    SOURCEDIR=$1
    IMAGENAME=wil1/remote-development-stack/$1
    docker build -t $IMAGENAME $SOURCEDIR 
    docker tag $IMAGENAME registry.gitlab.com/$IMAGENAME 
    docker push registry.gitlab.com/$IMAGENAME
}

buildAndPush ubuntu-xrdp
# buildAndPush socat-proxy
# buildAndPush code-server

This is based on: https://github.com/danielguerra69/ubuntu-xrdp

## Ubuntu 18.04/16.04  Multi User Remote Desktop Server

Fully implemented Multi User xrdp
with xorgxrdp and pulseaudio
on Ubuntu 16.04/18.04.
Copy/Paste and sound is working.
Users can re-login in the same session.
Xfce4, Firefox are pre installed.


(WARNING: use minimum --shm-size 1g or firefox/chrome will crash)

## Sample user

There is a sample user with sudo rights

Username: ubuntu
Password: ubuntu


## Add new users

No configuration is needed for new users just do

```bash
docker exec -ti uxrdp adduser mynewuser
```

After this the new user can login

## Add new services

To make sure all processes are working supervisor is installed.
The location for services to start is /etc/supervisor/conf.d

Example: Add mysql as a service

```bash
apt-get -yy install mysql-server
echo "[program:mysqld] \
command= /usr/sbin/mysqld \
user=mysql \
autorestart=true \
priority=100" > /etc/supervisor/conf.d/mysql.conf
supervisorctl update
```

## Volumes
This image uses two volumes:
1. `/etc/ssh/` holds the sshd host keys and config
2. `/home/` holds the `ubuntu/` default user home directory

When bind-mounting `/home/`, make sure it contains a folder `ubuntu/` with proper permission, otherwise no login will be possible.

```
mkdir -p ubuntu
chown 999:999 ubuntu
```

## Installing additional packages during build

The Dockerfile has support for the build argument ADDITIONAL_PACKAGES to install additional packages during build. Either pass it with `--build-arg` during `docker build` or add it 
as `args` in your `docker-compose.override.yml` and run `docker-compose build`.

